import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RestaurantesService {

  constructor(private _http: Http) { }

  getRestaurantes() {
    return this._http.get('http://localhost/slim/restaurantes-api.php/restaurantes')
      .map(res => res.json());
  }
}
