import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html',
  styleUrls: ['./empleados.component.css']
})
export class EmpleadosComponent implements OnInit {
  public titulo: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.titulo = "Empleados";
  }

  ngOnInit() {
  }

  empresas(){
      this.router.navigate(['/empresas', 'nombre-custom']);
    }
}
