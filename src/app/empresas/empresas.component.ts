import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { RestaurantesService } from '../restaurantes.service'

@Component({
  selector: 'app-empresas',
  templateUrl: './empresas.component.html',
  styleUrls: ['./empresas.component.css'],
  providers: [RestaurantesService]
})
export class EmpresasComponent implements OnInit {
  public titulo: string;
  public nombre: string;
  public restaurantes;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _restaurantesService: RestaurantesService
  ) {
    this.titulo = "Empresas";
    this._restaurantesService.getRestaurantes().subscribe(
      result => {
        console.log(result);
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      this.nombre = params['nombre'];
    });
  }

  empleados() {
    this.router.navigate(['/empleados']);
  }


}
