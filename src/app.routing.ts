import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmpleadosComponent } from '../src/app/empleados/empleados.component';
import { EmpresasComponent } from '../src/app/empresas/empresas.component';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/empresas',
        pathMatch: 'full'
    },
    { path:'empresas', component: EmpresasComponent},
    { path:'empleados', component: EmpleadosComponent},
    { path:'empresas/:nombre', component: EmpresasComponent}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
